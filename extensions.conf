[general]
static=yes
writeprotect=no
clearglobalvars=no
userscontext=fedorapeople
DYNAMIC_FEATURES=automon

[globals]

[macro-stdexten];
;
; Standard extension macro:
;   ${ARG1} - Extension  (we could have used ${MACRO_EXTEN} here as well
;   ${ARG2} - Device(s) to ring
;
exten => s,1,Dial(${ARG2},20)                   ; Ring the interface, 20 seconds maximum
exten => s,2,Goto(s-${DIALSTATUS},1)            ; Jump based on status (NOANSWER,BUSY,CHANUNAVAIL,CONGESTION,ANSWER)

exten => s-NOANSWER,1,Voicemail(${ARG1}@fedorapeople,u)      ; If unavailable, send to voicemail w/ unavail announce
exten => s-NOANSWER,2,Hangup

exten => s-BUSY,1,Voicemail(${ARG1}@fedorapeople,b)          ; If busy, send to voicemail w/ busy announce
exten => s-BUSY,2,Hangup

exten => _s-.,1,Goto(s-NOANSWER,1)              ; Treat anything else as no answer

exten => a,1,VoicemailMain(${ARG1})             ; If they press *, send the user into VoicemailMain

[conferences]
exten => s,1,Answer()
exten => s,n,Set(count=0)
exten => s,n(readconfno),Set(count=$[${count} + 1])
exten => s,n,Read(confno,conf-getconfno,0,,,15)
exten => s,n,GotoIf($[${LEN(${confno})} = 5]?checkconf:invalidconfno)

exten => s,n(invalidconfno),GotoIf($[${count} >= 3]?goodbye)
exten => s,n,Background(conf-invalid)
exten => s,n,Goto(readconfno)

exten => s,n(checkconf),GotoIf($[${DB_EXISTS(conferences/${confno})} = 0]?invalidconfno)

exten => s,n,Set(confpin=${DB(conferences/${confno})})
exten => s,n,GotoIf($[${confpin} = NONE]?startconf) ; if no pass, jump directly to conf

exten => s,n,Set(count=0)
exten => s,n(readpin),Set(count=$[${count} + 1])
exten => s,n,Read(userpin,conf-getpin,0,,,15)
exten => s,n,GotoIf($["${userpin}" = "${confpin}"]?startconf)
exten => s,n(invalidpin),GotoIf($[${count} >= 3]?goodbye)
exten => s,n,Background(conf-invalidpin)
exten => s,n,Goto(readpin)

exten => s,n(startconf),Background(conf-placeintoconf)
exten => s,n,SayDigits(${confno})
exten => s,n,Conference(${confno},,beep,beep)

exten => s,n(goodbye),Background(vm-goodbye)
exten => s,n,Hangup

[fedoraconferences]
; Wrapper for infrastructure so we can record
; Monitor only works on two channels bridged together

exten => infrastructure,1,Answer()
exten => infrastructure,n,Dial(Local/infrastructure_monitor@fedoraconferences/n,,wW)
exten => infrastructure_monitor,1,Set(DYNAMIC_FEATURES=automon)
exten => infrastructure_monitor,n,Set(TOUCH_MONITOR_FORMAT=ogg)
exten => infrastructure_monitor,n,Conference(infrastructure_monitor,,beep,beep)
exten => 2001,1,Goto(infrastructure,1)

exten => fudcon,1,Conference(fudcon,,beep,beep)
exten => 2002,1,Goto(fudcon,1)
exten => board-test,1,Answer()
exten => board-test,n,Wait(1.0)
exten => board-test,n,Authenticate(1234)
exten => board-test,n,Conference(board-test,,beep,beep)
exten => 2003,1,Goto(board-test,1)
exten => marketing,1,Conference(marketing,,beep,beep)
exten => 2004,1,Goto(marketing,1)
exten => fpl,1,Conference(fpl,,beep,beep)
exten => 2005,1,Goto(fpl,1)
exten => fadna,1,Conference(fad,,beep,beep)
exten => 2006,1,Goto(fad,1)
exten => cast,1,Conference(cast,,beep,beep)
exten => 2007,1,Goto(cast,1)
exten => docs,1,Conference(docs,,beep,beep)
exten => 2008,1,Goto(docs,1)
exten => fpm,1,Conference(fpm,,beep,beep)
exten => 2009,1,Goto(fpm,1)
exten => ambassadors,1,Conference(ambassadors,,beep,beep)
exten => 2015,1,Goto(ambassadors,1)

; Conference rooms not assigned to any person or project, for free use
exten => conf0,1,Conference(conf1,,beep,beep)
exten => 2010,1,Goto(conf0,1)
exten => conf1,1,Conference(conf1,,beep,beep)
exten => 2011,1,Goto(conf1,1)
exten => conf2,1,Conference(conf1,,beep,beep)
exten => 2012,1,Goto(conf2,1)
exten => conf3,1,Conference(conf1,,beep,beep)
exten => 2013,1,Goto(conf3,1)
exten => conf4,1,Conference(conf1,,beep,beep)
exten => 2014,1,Goto(conf4,1)


[from-pstn]

exten => 9194240063,1,Goto(directory,s,1)	; NC
exten => 3125770052,1,Goto(directory,s,1)	; Ill
exten => 9783038021,1,Goto(directory,s,1) ; Mass
exten => 6509309514,1,Goto(directory,s,1) ; California
exten => 442030518327,1,Goto(directory,s,1) ; UK
exten => 44203051832,1,Goto(directory,s,1) ; (last digit was missing on DID)

[directory]
exten => s,1,Answer(1000)
exten => s,n,Set(LoopCount=0)
exten => s,n,Background(thanks-for-calling-today)
exten => s,n(loop),While($[${LoopCount} <= 3])
exten => s,n,Set(LoopCount=$[${LoopCount} + 1])
exten => s,n,Background(enter-ext-of-person)
exten => s,n,Background(to-dial-by-name-press&digits/8)
exten => s,n,WaitExten()
exten => s,n(endloop),EndWhile()
exten => s,n,Playback(connection-timed-out)
exten => s,n,Playback(vm-goodbye)
exten => s,n,Hangup()

exten => 8,1,Directory(fedorapeople,from-contributor,fe)

include => fedorapeople
include => fedorapeople-iax2
include => fedoraconferences

[from-contributor]

include => fedorapeople
include => fedorapeople-iax2
include => fedoraconferences

exten => directory,1,Goto(directory,s,1)
exten => 1000,1,Goto(directory,s,1)

exten => louie,1,Answer
exten => louie,n,Wait(1.0)
exten => louie,n,Playback(lyrics-louie-louie)
exten => 1001,1,Goto(louie,1)

exten => echo,1,Answer
exten => echo,n,Echo
exten => echo,n,Hangup
exten => 1002,1,Goto(echo,1)

[fedorapeople-iax2]

exten => dgilmore,1,Dial(IAX2/dgilmore)

[conference-stream-join-conference]

exten => s,1,Set(GROUP(conference-streaming)=${conference})
exten => s,n,GotoIf($[${GROUP(${conference}@conference-streaming)} > 1]?done)
exten => s,n,Answer()
exten => s,n,Conference(${conference},,fedora/this-conference-call-is-now-being-recorded,fedora/this-conference-call-is-no-longer-being-recorded)
exten => s,n,Playback(fedora/this-conference-call-is-no-longer-being-recorded)
exten => s,n,Wait(5.0)
exten => s,n(done),Hangup()

[conference-stream-start-streaming]

exten => s,1,Answer()
exten => s,n,ICES(${conference}-ices.xml)
exten => s,n,Hangup()

[conference-record-join-conference]

exten => s,1,Set(GROUP(conference-record)=${conference})
exten => s,n,GotoIf($[${GROUP_COUNT(${conference}@conference-record)} > 1]?done)
exten => s,n,Answer()
exten => s,n,Conference(${conference},,fedora/this-conference-call-is-now-being-recorded,fedora/this-conference-call-is-no-longer-being-recorded)
exten => s,n,Playback(fedora/this-conference-call-is-no-longer-being-recorded)
exten => s,n,Wait(5.0)
exten => s,n(done),Hangup()

[conference-record-start-recording]

exten => s,1,Answer()
exten => s,n,Record(/var/spool/asterisk/monitor/conference-${conference}-${STRFTIME(${EPOCH},UTC,%Y-%m-%d-%H-%M-%S)}.ogg,,${timelimit},qx)
exten => s,n,Hangup()
